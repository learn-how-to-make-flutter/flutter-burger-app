import 'package:flutter/material.dart';

import 'Foodmenu.dart';

class Menu_page extends StatefulWidget {
  static const tag = "menu_page";

  @override
  State<Menu_page> createState() => _Menu_pageState();
}

class _Menu_pageState extends State<Menu_page> {
  List<FoodMenu> menu = [
    FoodMenu("ผัดไทย", "50", "images/food_pictures_3.jpg"),
    FoodMenu("กะเพรา", "40", "images/food_pictures_4.jpg"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("เลือกเมนูอาหาร "),
        ),
        body: ListView.builder(
            itemCount: menu.length,
            itemBuilder: (BuildContext context, int index) {
              FoodMenu food = menu[index];
              return ListTile(
                leading: Image.asset(food.img),
                title: Text(
                  food.name,
                  style: TextStyle(fontSize: 23),
                ),
                subtitle: Text("ราคา " + food.price + " บาท"),
                onTap: () {
                  print("เลือกเมนู = " + food.name + " " + food.price + " บาท");
                },
              );
            }));
  }
}
